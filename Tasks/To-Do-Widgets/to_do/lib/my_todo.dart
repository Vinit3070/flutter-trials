import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class MyToDo extends StatefulWidget {
  const MyToDo({Key? key}) : super(key: key);

  @override
  State createState() => _MyToDoState();
}

class ToDoModelClass {
  final String title;
  final String description;
  final String date;

  ToDoModelClass({
    required this.title,
    required this.description,
    required this.date,
  });
}

class _MyToDoState extends State<MyToDo> {
  TextEditingController titleCont = TextEditingController();
  TextEditingController descCont = TextEditingController();
  TextEditingController dateCont = TextEditingController();

  int flag = 0;
  bool titleVal = false;
  bool descVal = false;
  bool dateVal = false;

  @override
  void dispose() {
    titleCont.dispose();
    descCont.dispose();
    dateCont.dispose();

    super.dispose();
  }

  List<ToDoModelClass> card_list = [
    ToDoModelClass(
      title: "Match : Mumbai vs Chennai",
      description: "This is the qualifier match between Mi and Csk and winner team directly goes to the final",
      date: "Mar 20 2024",
    ),
    ToDoModelClass(
      title: "App Submission",
      description: "Submitting the Quiz App",
      date: "Mar 25 14 2024",
    ),
    ToDoModelClass(
      title: "Match : Mumbai vs Bangalore",
      description: "This is the Eliminator match between Mi and Rcb",
      date: "Mar 30 2024",
    ),
    // ToDoModelClass(
    //   title: "New App Submission",
    //   description: "Submitting the to do App",
    //   date: "Apr 1 2024",
    // ),
  ];

  List<Color> color_list = const [
    Color.fromRGBO(252, 232, 220, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1)
  ];

  void editBtmsheet(ToDoModelClass obj, int index) {
    titleCont.text = obj.title;
    descCont.text = obj.description;
    dateCont.text = obj.date;

    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text(
                      "Edit Task",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 22,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                  child: const Text(
                    "Title",
                    style: TextStyle(
                      fontSize: 11,
                      color:Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: titleCont,
                    decoration: InputDecoration(
                      hintText: "Enter task",
                      errorText: titleVal ? 'Value Cannot Be Empty' : null,
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 139, 148, 1),
                          width: 2.0,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                  child: const Text(
                    "Description",
                    style: TextStyle(
                      fontSize: 11,
                      color:  Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 80,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: descCont,
                    expands: true,
                    maxLines: null,
                    decoration: InputDecoration(
                      hintText: "Enter Details",
                      errorText: descVal ? 'Value Cannot Be Empty' : null,
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 139, 148, 1),
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                  child: const Text(
                    "Date",
                    style: TextStyle(
                      fontSize: 11,
                      color:  Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: dateCont,
                    decoration: InputDecoration(
                      errorText: dateVal ? 'Value Cannot Be Empty' : null,
                      suffixIcon: const Icon(Icons.calendar_month_rounded),
                      hintText: "Select Date",
                    ),
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2023),
                        lastDate: DateTime(2100),
                      );

                      if (pickedDate != null) {
                        String formattedDate = DateFormat.yMMMd().format(pickedDate);
                        setState(() {
                          dateCont.text = formattedDate;
                        });
                      }
                    },
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: SizedBox(
                    height: 50,
                    width: 300,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            if (titleCont.text.isEmpty) {
                              titleVal = true;
                            } else {
                              titleVal = false;
                            }
                            if (descCont.text.isEmpty) {
                              descVal = true;
                            } else {
                              descVal = false;
                            }
                            if (dateCont.text.isEmpty) {
                              dateVal = true;
                            } else {
                              dateVal = false;
                            }

                            if (!titleVal && !descVal && !dateVal) {
                              card_list[index] = ToDoModelClass(
                                title: titleCont.text,
                                description: descCont.text,
                                date: dateCont.text,
                              );
                              Navigator.of(context).pop();
                              titleCont.clear();
                              descCont.clear();
                              dateCont.clear();
                            }
                          });
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
                          foregroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          "Update",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Future addBtmsheet() {
    return showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text(
                      "Create Task",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 22,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                  child: const Text(
                    "Title",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: titleCont,
                    decoration: InputDecoration(
                      hintText: "Enter task",
                      errorText: titleVal ? 'Value Cannot Be Empty' : null,
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 139, 148, 1),
                          width: 2.0,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                  child: const Text(
                    "Description",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color:  Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 80,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: descCont,
                    expands: true,
                    maxLines: null,
                    decoration: InputDecoration(
                      hintText: "Enter Details",
                      errorText: descVal ? 'Value Cannot Be Empty' : null,
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 139, 148, 1),
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                  child: const Text(
                    "Date",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                      color:  Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: dateCont,
                    decoration: InputDecoration(
                      errorText: dateVal ? 'Value Cannot Be Empty' : null,
                      suffixIcon: const Icon(Icons.calendar_month_rounded),
                      hintText: "Select Date",
                    ),
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2023),
                        lastDate: DateTime(2100),
                      );

                      if (pickedDate != null) {
                        String formattedDate = DateFormat.yMMMd().format(pickedDate);
                        setState(() {
                          dateCont.text = formattedDate;
                        });
                      }
                    },
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Center(
                  child: SizedBox(
                    height: 50,
                    width: 300,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            if (titleCont.text.isEmpty) {
                              titleVal = true;
                            } else {
                              titleVal = false;
                            }
                            if (descCont.text.isEmpty) {
                              descVal = true;
                            } else {
                              descVal = false;
                            }
                            if (dateCont.text.isEmpty) {
                              dateVal = true;
                            } else {
                              dateVal = false;
                            }

                            if (!titleVal && !descVal && !dateVal) {
                              card_list.add(ToDoModelClass(
                                title: titleCont.text,
                                description: descCont.text,
                                date: dateCont.text,
                              ));
                              titleCont.clear();
                              descCont.clear();
                              dateCont.clear();
                              Navigator.pop(context);
                            }
                          });
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromARGB(255, 116, 223, 207),
                          foregroundColor: Colors.black,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          "Submit",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(
            Icons.menu,
            size: 30,
            color: Colors.black,
            weight: 50.0,
          ),
        ),
        title: Container(
          alignment: Alignment.centerLeft,
          child: Text(
            "To-do List",
            style: GoogleFonts.quicksand(
              textStyle: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 26,
              ),
            ),
          ),
        ),
        shadowColor: Colors.white,
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 116, 223, 207),
      ),
      body: Container(
        height: 900,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end:Alignment.bottomLeft,
            colors: 
            [
               Color.fromRGBO(19, 42, 59, 1),
               Color.fromRGBO(255, 255, 255,1),
               Color.fromRGBO(5, 44, 83, 1),
            ],
          ),
          // image: DecorationImage(
          //   image: AssetImage("images/backg.jpg",), // Change this to your background image path
          //   fit: BoxFit.fill,
          // ),
        ),
        child: ListView.builder(
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(),
          itemCount: card_list.length,
          itemBuilder: ((context, index) {
            return Padding(
              padding: const EdgeInsets.only(top: 25.0, bottom: 5.0),
              child: Container(
                height: 125,
                width: 330,
                margin: const EdgeInsets.symmetric(horizontal: 15.5),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  color: color_list[index % color_list.length],
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      offset: Offset(0, 8),
                      blurRadius: 8,
                      spreadRadius: 1,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 90),
                            child: SizedBox(
                              height: 15,
                              width: 243,
                              child: Text(
                                card_list[index].title,
                                style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Container(
                            height: 52,
                            width: 52,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(30)),
                              color: Color.fromARGB(255, 255, 255, 255),
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.07),
                                  offset: Offset(0, 0),
                                  blurRadius: 8,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: Image.asset(
                              "images/image1.png",
                              height: 19,
                              width: 23,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8.0,
                            left: 20.0,
                            bottom: 0.0,
                            right: 0.0,
                          ),
                          child: Container(
                            height: 43,
                            width: 245,
                            child: Text(
                              card_list[index].description,
                              style: GoogleFonts.quicksand(
                                textStyle: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 10,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 13,
                                width: 100,
                                child: Text(
                                  card_list[index].date,
                                  style: GoogleFonts.quicksand(
                                    textStyle: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(right: 0.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Column(
                                children: [
                                  IconButton(
                                    onPressed: () {
                                      editBtmsheet(card_list[index], index);
                                    },
                                    iconSize: 17,
                                     color:
                                          const Color.fromRGBO(0, 139, 148, 1),
                                    icon: const Icon(Icons.edit),
                                  ),
                                ],
                              ),
                               Column(
                                children: [
                                  IconButton(
                                      onPressed: () {

                                        showAlertDialog(BuildContext context) {

                                            Widget cancelButton = TextButton(
                                              child: const Text("Cancel"),
                                              onPressed:  () {
                                                setState(() {
                                                  Navigator.of(context).pop();
                                                });
                                              },
                                            );

                                            Widget deleteButton = TextButton(
                                              child: const Text("Delete"),
                                              onPressed:  () {
                                                setState(() {
                                                card_list.removeAt(index);
                                                Navigator.of(context).pop();
                                              });
                                              },
                                            );

                                            AlertDialog alert = AlertDialog(
                                              title: const Text(
                                                "Alert",
                                                style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500),
                                              ),
                                              content: const Text("Are you sure for delete..?"),
                                              actions: [
                                                cancelButton,
                                                deleteButton,
                                              ],
                                            );
                                            showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return alert;
                                              },
                                            );
                                            
                                          }

                                          setState(() {
                                            showAlertDialog(context);
                                            
                                          });
                                      
                                        
                                      },
                                      iconSize: 17,
                                      color:
                                          const Color.fromRGBO(0, 139, 148, 1),
                                      icon: const Icon(Icons.delete_rounded)),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          }),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addBtmsheet,
        child: const Icon(
          Icons.add,
          size: 40,
          color: Color.fromRGBO(0, 139, 148, 1),
        ),
      ),
    );
  }
}



