import 'package:flutter/material.dart';

class wid1 extends StatefulWidget{

  const wid1({super.key});

  State createState()=> _newWidState();
}

class _newWidState extends State{

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text("Demo"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          height:200,
          width:200,
          decoration: BoxDecoration(
            color: Colors.blue,
            border: Border.all(width:4,color: Colors.black),
              borderRadius: const BorderRadius.all(Radius.circular(150),
            ),
            boxShadow: const[
              BoxShadow(
                color: Colors.red,
                offset: Offset(0, 0),
                blurRadius: 30,
              ),
            ],
          ),
          child: Image.network("https://s.hdnux.com/photos/47/46/46/10381742/3/rawImage.jpg",),
        ),
      ),
    
    );
  }

}