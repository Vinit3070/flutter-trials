import 'package:flutter/material.dart';
// import 'package:to_do/wid1.dart';
import 'package:to_do/my_todo.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      // home: wid1(),
      home: MyToDo(),
      debugShowCheckedModeBanner: false,
    );
  }
}


 
