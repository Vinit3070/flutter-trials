import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Bottom(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Bottom extends StatefulWidget {
  const Bottom({super.key});

  @override
  State createState() => _BottomState();
}

class _BottomState extends State {
  TextEditingController titleCont = TextEditingController();
  TextEditingController descCont = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("To-Do App"),
        centerTitle: true,
        backgroundColor: Colors.cyan,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: Text(
                            "Create Task",
                            style: GoogleFonts.quicksand(
                                fontSize: 22, fontWeight: FontWeight.w600),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        //Title
                        Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: Text(
                            "Title",
                            style: GoogleFonts.quicksand(
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 50,
                          width: double.infinity,
                          margin: const EdgeInsets.fromLTRB(10, 2, 10, 0),
                          child: TextField(
                            controller: titleCont,
                            decoration: const InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromRGBO(0, 139, 148, 1),
                                  width: 2.0,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                              ),
                              hintText: " Enter Task Title ",
                              hintStyle: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.w400),
                            ),
                            keyboardType: TextInputType.multiline,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        //Description
                        Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: Text(
                            "Description",
                            style: GoogleFonts.quicksand(
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 80,
                          width: double.infinity,
                          margin: const EdgeInsets.fromLTRB(10, 2, 10, 0),
                          child: TextField(
                            controller: descCont,
                            expands: true,
                            maxLines: null,
                            decoration: const InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromRGBO(0, 139, 148, 1),
                                  width: 2.0,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                              ),
                              hintText: " Enter Description ",
                              hintStyle: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.w400),
                            ),
                            keyboardType: TextInputType.multiline,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        //Date
                        Container(
                          margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: Text(
                            "Date",
                            style: GoogleFonts.quicksand(
                              fontSize: 10,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 50,
                          width: double.infinity,
                          margin: const EdgeInsets.fromLTRB(10, 2, 10, 0),
                          child: TextField(
                            controller: titleCont,
                            decoration: const InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromRGBO(0, 139, 148, 1),
                                  width: 2.0,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                              ),
                              hintText: " Enter Date ",
                              hintStyle: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.w400),
                            ),
                            keyboardType: TextInputType.multiline,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        //button
                        Container(
                          alignment: Alignment.center,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                fixedSize: const Size(150, 50),
                                backgroundColor:
                                    Color.fromARGB(255, 115, 208, 241),
                                foregroundColor:
                                    const Color.fromARGB(255, 249, 250, 250),
                                textStyle: const TextStyle(
                                    fontSize: 15,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w600),
                              ),
                              onPressed: () {},
                              child: const Text("Submit")),
                        )
                      ],
                    ),
                  ),
                );
              });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
