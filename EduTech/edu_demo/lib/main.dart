// ignore_for_file: unused_import

import 'package:edu_demo/EduTech.dart';
import 'package:edu_demo/MyEduTech.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(const MyApp());
}

class MyApp extends StatelessWidget{

  const MyApp({super.key});

  @override
  Widget build (BuildContext context){

    return const MaterialApp(

      home:EduTech(),
      debugShowCheckedModeBanner: false,
    );
  }
}

