// ignore_for_file: sized_box_for_whitespace, avoid_unnecessary_containers, file_names

import 'package:edu_demo/MyEduTech.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class EduTech extends StatefulWidget {
  const EduTech({super.key});

  @override
  State createState() => _EduTechState();
}

class _EduTechState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 213, 226, 226),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 30,
            width: double.infinity,
            padding: const EdgeInsets.only(left: 10, right: 10),
            margin: const EdgeInsets.only(top: 47),
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const MyEduTech()));
                  },
                  icon: const Icon(Icons.notes),
                  iconSize: 26,
                ),
                const Spacer(),
                IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.notifications),
                    iconSize: 26)
              ],
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Container(
            padding: const EdgeInsets.only(left: 20, top: 19),
            width: double.infinity,
            height: 130,
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Welcome to New",
                      style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                        fontSize: 27,
                        color: Color.fromRGBO(0, 0, 0, 1),
                      )),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text("Educourse",
                        style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                            fontSize: 37,
                            color: Color.fromRGBO(0, 0, 0, 1),
                            fontWeight: FontWeight.w700,
                          ),
                        ))
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Container(
            height: 57,
            width: double.infinity,
            margin: const EdgeInsets.only(left: 20, right: 20, bottom: 29),
            child: const TextField(
              decoration: InputDecoration(
                  hintText: "Enter Keyword",
                  filled: true,
                  fillColor: Color.fromRGBO(255, 255, 255, 1),
                  suffixIcon: Icon(
                    Icons.search_outlined,
                    size: 27,
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(Radius.circular(28.5)))),
              keyboardType: TextInputType.multiline,
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
                child: Container(
              padding: const EdgeInsets.only(
                  top: 15, bottom: 20, left: 20, right: 20),
              height: 540,
              width: double.infinity,
              decoration: const BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(38),
                      topRight: Radius.circular(38))),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Course For You",
                        style: GoogleFonts.jost(
                            textStyle: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 17,
                                color: Color.fromRGBO(0, 0, 0, 1))),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                    ],
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(right: 20, top: 16),
                          width: 200,
                          height: 242,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(197, 4, 98, 1),
                                Color.fromRGBO(80, 3, 112, 1)
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomLeft,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 25, top: 20),
                                child: Text(
                                  "UX Designer from Scratch.",
                                  style: GoogleFonts.jost(
                                    textStyle: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Image.asset(
                                "images/c1.png",
                                height: 160,
                                width: 160,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(right: 20, top: 16),
                          width: 190,
                          height: 242,
                          decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(0, 77, 228, 1),
                                  Color.fromRGBO(1, 47, 135, 1)
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomLeft,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(14))),
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 25, top: 20),
                                child: Text(
                                  "Design Thinking The Beginner",
                                  style: GoogleFonts.jost(
                                    textStyle: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Image.asset(
                                "images/c2.png",
                                height: 160,
                                width: 160,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(right: 20, top: 16),
                          width: 200,
                          height: 242,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromRGBO(197, 4, 98, 1),
                                Color.fromRGBO(80, 3, 112, 1)
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomLeft,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 25, top: 20),
                                child: Text(
                                  "UX Designer from Scratch.",
                                  style: GoogleFonts.jost(
                                    textStyle: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Image.asset(
                                "images/c1.png",
                                height: 160,
                                width: 160,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Course By Category",
                        style: GoogleFonts.jost(
                          textStyle: const TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(0, 0, 0, 1)),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SingleChildScrollView(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Container(
                                height: 36,
                                width: 36,
                                decoration: const BoxDecoration(
                                  color: Color.fromRGBO(25, 0, 56, 1),
                                ),
                                child: Image.asset("images/img1.png"),
                              ),
                              const SizedBox(
                                height: 9,
                              ),
                              Text(
                                "UI/UX",
                                style: GoogleFonts.jost(
                                    textStyle: const TextStyle(fontSize: 14)),
                              )
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                height: 36,
                                width: 36,
                                decoration: const BoxDecoration(
                                  color: Color.fromRGBO(25, 0, 56, 1),
                                ),
                                child: Image.asset("images/img2.png"),
                              ),
                              const SizedBox(
                                height: 9,
                              ),
                              Text(
                                "VISUAL",
                                style: GoogleFonts.jost(
                                    textStyle: const TextStyle(fontSize: 14)),
                              )
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                height: 36,
                                width: 36,
                                decoration: const BoxDecoration(
                                  color: Color.fromRGBO(25, 0, 56, 1),
                                ),
                                child: Image.asset("images/img3.png"),
                              ),
                              const SizedBox(
                                height: 9,
                              ),
                              Text(
                                "Illustration",
                                style: GoogleFonts.jost(
                                    textStyle: const TextStyle(fontSize: 14)),
                              )
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                height: 36,
                                width: 36,
                                decoration: const BoxDecoration(
                                  color: Color.fromRGBO(25, 0, 56, 1),
                                ),
                                child: Image.asset("images/img4.png"),
                              ),
                              const SizedBox(
                                height: 9,
                              ),
                              Text(
                                "PHOTO",
                                style: GoogleFonts.jost(
                                    textStyle: const TextStyle(fontSize: 14)),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
          ),
        ],
      ),
    );
  }
}
