// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:edu_demo/EduTech.dart';
import 'package:google_fonts/google_fonts.dart';

class MyEduTech extends StatefulWidget {
  const MyEduTech({super.key});

  @override
  State createState() => _MyEduTechState();
}

class _MyEduTechState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(80, 3, 112, 1),
        body: Column(
          children: [
            Container(
              height: 320,
              width: double.infinity,
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                    Color.fromRGBO(80, 3, 112, 1),
                    Color.fromRGBO(197, 4, 98, 1),
                  ])),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const EduTech()));
                    },
                    icon: const Icon(Icons.arrow_back),
                    color: Colors.white,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30),
                    child: SizedBox(
                      height: 150,
                      width: 300,
                      child: Column(
                        children: [
                          Text(
                            "UX Designer from Scratch",
                            style: GoogleFonts.jost(
                                textStyle: const TextStyle(
                              fontSize: 32,
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            )),
                          ),
                          const SizedBox(
                            height: 11,
                          ),
                          Text(
                            "Basic guideline & tips & tricks for how to become a UX designer easily.",
                            style: GoogleFonts.jost(
                                textStyle: const TextStyle(
                              fontSize: 16,
                              color: Color.fromRGBO(228, 205, 225, 1),
                              fontWeight: FontWeight.w400,
                            )),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25.0, top: 30),
                    child: Row(children: [
                      SizedBox(
                        height: 35,
                        width: 170,
                        child: Row(
                          children: [
                            Container(
                              height: 32,
                              width: 32,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(width: 1, color: Colors.white),
                                color: const Color.fromRGBO(0, 82, 178, 1),
                              ),
                              child: const Icon(
                                Icons.person_2_outlined,
                                size: 30,
                                color: Colors.white,
                              ),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(" Author :",
                                style: GoogleFonts.jost(
                                  textStyle: const TextStyle(
                                    fontSize: 16,
                                    color: Color.fromRGBO(187, 148, 195, 1),
                                    fontWeight: FontWeight.w400,
                                  ),
                                )),
                            Text(" Jenny ",
                                style: GoogleFonts.jost(
                                  textStyle: const TextStyle(
                                    fontSize: 16,
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontWeight: FontWeight.w500,
                                  ),
                                )),
                          ],
                        ),
                      ),
                      const Spacer(),
                      Padding(
                        padding: const EdgeInsets.only(right: 5.0),
                        child: SizedBox(
                          width: 160,
                          height: 35,
                          child: Row(
                            children: [
                              Text("4.8 ",
                                  style: GoogleFonts.jost(
                                    textStyle: const TextStyle(
                                      fontSize: 16,
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      fontWeight: FontWeight.w500,
                                    ),
                                  )),
                              Image.asset(
                                "images/star.png",
                                height: 16,
                                width: 16,
                              ),
                              Text(" (20 review)",
                                  style: GoogleFonts.jost(
                                    textStyle: const TextStyle(
                                      fontSize: 16,
                                      color: Color.fromRGBO(187, 148, 195, 1),
                                      fontWeight: FontWeight.w500,
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(38),
                    topLeft: Radius.circular(38),
                  ),
                ),
                child: addListViewBuilder(),
              ),
            ),
          ],
        ));
  }

  Widget addListViewBuilder() {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Container(
            height: 80,
            width: 300,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: Color.fromRGBO(255, 255, 255, 1),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.15),
                  offset: Offset(0, 8),
                  blurRadius: 5,
                  spreadRadius: 0.6,
                ),
              ],
            ),
            child: Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 18.0),
                      child: Container(
                        height: 60,
                        width: 50,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          color: Color.fromRGBO(230, 239, 239, 1),
                        ),
                        child: Image.asset(
                          "images/youtube.png",
                          height: 25,
                          width: 25,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 6,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        child: Text("Introduction",
                            style: GoogleFonts.jost(
                              textStyle: const TextStyle(
                                fontSize: 17,
                                color: Color.fromRGBO(0, 0, 0, 1),
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      SizedBox(
                        child: Text("Lorem Ipsum is simply dummy text ...",
                            style: GoogleFonts.jost(
                              textStyle: const TextStyle(
                                fontSize: 13,
                                color: Color.fromRGBO(143, 143, 143, 1),
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

