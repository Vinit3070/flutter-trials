// import 'package:flutter/material.dart';

// void main() => runApp(const MyApp());

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return const MaterialApp(
//       home: QuizApp(),
//       debugShowCheckedModeBanner: false,
//     );
//   }
// }

// class QuizApp extends StatefulWidget {
//   const QuizApp({Key? key}) : super(key: key);

//   @override
//   State<QuizApp> createState() => _QuizAppState();
// }

// class _QuizAppState extends State<QuizApp> {

//   List<Map<String, dynamic>> allQuestions = [
//     {
//       "question": "Who is the founder of Microsoft?",
//       "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
//       "answerIndex": 2,
//     },
//     {
//       "question": "Who is the founder of Apple?",
//       "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
//       "answerIndex": 0,
//     },
//     {
//       "question": "Who is the founder of Amazon?",
//       "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
//       "answerIndex": 1,
//     },
//     {
//       "question": "Who is the founder of Tesla?",
//       "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "Elon Musk"],
//       "answerIndex": 3,
//     },
//     {
//       "question": "Who is the founder of Google?",
//       "options": ["Steve Jobs", "Larry Page", "Bill Gates", "Elon Musk"],
//       "answerIndex": 1,
//     },
//   ];

//   bool questionScreen = true;
//   int questionIndex = 0;

//   bool isbtn1Pressed = false;
//   bool isbtn2Pressed = false;
//   bool isbtn3Pressed = false;
//   bool isbtn4Pressed = false;

//   Color changeb1() {
//     if (isbtn1Pressed) {
//       return allQuestions[questionIndex]["answerIndex"] == 0 ? Colors.green : Colors.red;
//     } else {
//       return Colors.white;
//     }
//   }

//   Color changeb2() {
//     if (isbtn2Pressed) {
//       return allQuestions[questionIndex]["answerIndex"] == 1 ? Colors.green : Colors.red;
//     } else {
//       return Colors.white;
//     }
//   }

//   Color changeb3() {
//     if (isbtn3Pressed) {
//       return allQuestions[questionIndex]["answerIndex"] == 2 ? Colors.green : Colors.red;
//     } else {
//       return Colors.white;
//     }
//   }

//   Color changeb4() {
//     if (isbtn4Pressed) {
//       return allQuestions[questionIndex]["answerIndex"] == 3 ? Colors.green : Colors.red;
//     } else {
//       return Colors.white;
//     }
//   }

//   void updateButtonStates(int index) {
//     setState(() {
//       isbtn1Pressed = index == 0;
//       isbtn2Pressed = index == 1;
//       isbtn3Pressed = index == 2;
//       isbtn4Pressed = index == 3;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text(
//           "Quiz App",
//           style: TextStyle(
//             fontSize: 30,
//             fontWeight: FontWeight.w800,
//             color: Colors.orange,
//           ),
//         ),
//         centerTitle: true,
//         backgroundColor: Colors.blue,
//       ),
//       body: Column(
//         children: [
//           const SizedBox(
//             height: 25,
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Text(
//                 "Questions :  ${questionIndex + 1} / ${allQuestions.length} ",
//                 style: const TextStyle(
//                   fontSize: 25,
//                   fontWeight: FontWeight.w600,
//                 ),
//               ),
//             ],
//           ),
//           const SizedBox(
//             height: 50,
//           ),
//           SizedBox(
//             width: 380,
//             height: 50,
//             child: Text(
//               allQuestions[questionIndex]["question"],
//               style: const TextStyle(
//                 fontSize: 23,
//                 fontWeight: FontWeight.w400,
//               ),
//             ),
//           ),
//           const SizedBox(
//             height: 30,
//           ),
//           Container(
//             height: 50,
//             width: 350,
//             child: ElevatedButton(
//               onPressed: () {
//                 updateButtonStates(0);
//               },
//               style: ElevatedButton.styleFrom(
//                 backgroundColor: changeb1(),
//               ),
//               child: Text(
//                 "A.${allQuestions[questionIndex]["options"][0]}",
//                 style: const TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.w400,
//                 ),
//               ),
//             ),
//           ),
//           const SizedBox(
//             height: 20,
//           ),
//           Container(
//             height: 50,
//             width: 350,
//             child: ElevatedButton(
//               onPressed: () {
//                 updateButtonStates(1);
//               },
//               style: ElevatedButton.styleFrom(
//                 backgroundColor: changeb2(),
//               ),
//               child: Text(
//                 "B.${allQuestions[questionIndex]["options"][1]}",
//                 style: const TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.w400,
//                 ),
//               ),
//             ),
//           ),
//           const SizedBox(
//             height: 20,
//           ),
//           Container(
//             height: 50,
//             width: 350,
//             child: ElevatedButton(
//               onPressed: () {
//                 updateButtonStates(2);
//               },
//               style: ElevatedButton.styleFrom(
//                 backgroundColor: changeb3(),
//               ),
//               child: Text(
//                 "C.${allQuestions[questionIndex]["options"][2]}",
//                 style: const TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.w400,
//                 ),
//               ),
//             ),
//           ),
//           const SizedBox(
//             height: 20,
//           ),
//           Container(
//             height: 50,
//             width: 350,
//             child: ElevatedButton(
//               onPressed: () {
//                 updateButtonStates(3);
//               },
//               style: ElevatedButton.styleFrom(
//                 backgroundColor: changeb4(),
//               ),
//               child: Text(
//                 "D.${allQuestions[questionIndex]["options"][3]}",
//                 style: const TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.w400,
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () {
//           setState(() {
//             questionIndex++;
//             if (questionIndex >= allQuestions.length) {
//               questionIndex = 0;
//             }
//             // Reset button states
//             isbtn1Pressed = false;
//             isbtn2Pressed = false;
//             isbtn3Pressed = false;
//             isbtn4Pressed = false;
//           });
//         },
//         backgroundColor: Colors.blue,
//         child: const Icon(
//           Icons.forward,
//           color: Colors.orange,
//         ),
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: QuizApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});
  @override
  State createState() => _QuizAppState();
}

class _QuizAppState extends State {
  List<Map> allQuestions = [
    {
      "question": "Who is the founder of Microsoft?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "ElonMusk"],
      "answerIndex": 2,
    },
    {
      "question": "Who is the founder of Apple?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "ElonMusk"],
      "answerIndex": 0,
    },
    {
      "question": "Who is the founder of Amazon?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "EloMusk"],
      "answerIndex": 1,
    },
    {
      "question": "Who is the founder of Tesla?",
      "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "ElonMusk"],
      "answerIndex": 3,
    },
    {
      "question": "Who is the founder of Google?",
      "options": ["Steve Jobs", "Lary Page", "Bill Gates", "ElonMusk"],
      "answerIndex": 1,
    },
  ];
  bool questionScreen = true;
  int questionIndex = 0;
  int selectedAnswerIndex = -1;
  int noOfCorrectAnswers = 0;
  MaterialStateProperty<Color?> checkAnswer(int buttonIndex) {
    if (selectedAnswerIndex != -1) {
      if (buttonIndex == allQuestions[questionIndex]["answerIndex"]) {
        return const MaterialStatePropertyAll(Colors.green);
      } else if (buttonIndex == selectedAnswerIndex) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(null);
      }
    } else {
      return const MaterialStatePropertyAll(null);
    }
  }

  void validateCurrentPage() {
    if (selectedAnswerIndex == -1) {
      return;
    }
    if (selectedAnswerIndex == allQuestions[questionIndex]["answerIndex"]) {
      noOfCorrectAnswers += 1;
    }
    if (selectedAnswerIndex != -1) {
      if (questionIndex == allQuestions.length - 1) {
        setState(() {
          questionScreen = false;
        });
      }
      selectedAnswerIndex = -1;
      setState(() {
        questionIndex += 1;
      });
    }
  }

  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            "QuizApp",
          
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.orange,
            ),
          ),
          backgroundColor: Colors.blue,
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 25,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                const Text(
                  "Questions : ",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  "${questionIndex + 1}/${allQuestions.length}",
                  style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ]),
              const SizedBox(
                height: 50,
              ),
              SizedBox(
                width: 380,
                height: 50,
                child: Text(
                  allQuestions[questionIndex]["question"],
                  style: const TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(0),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 0;
                    });
                  }
                },
                child: Text(
                  "A.${allQuestions[questionIndex]["options"][0]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(1),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 1;
                    });
                  }
                },
                child: Text(
                  "B.${allQuestions[questionIndex]["options"][1]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(2),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 2;
                    });
                  }
                },
                child: Text(
                  "C.${allQuestions[questionIndex]["options"][2]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: checkAnswer(3),
                ),
                onPressed: () {
                  if (selectedAnswerIndex == -1) {
                    setState(() {
                      selectedAnswerIndex = 3;
                    });
                  }
                },
                child: Text(
                  "D.${allQuestions[questionIndex]["options"][3]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            validateCurrentPage();
          },
          backgroundColor: Colors.blue,
          child: const Icon(
            Icons.forward,
            color: Colors.orange,
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
            ),
            
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              Image.network(
                "https://img.freepik.com/premium-vector/winner-trophy-cup-with-ribbon-confetti_51486-122.jpg",
                height: 500,
                width: 380,
              ),
              const Text(
                "Congratulations!!!",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "You have completed the Quiz",
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 15),
              Text("$noOfCorrectAnswers/${allQuestions.length}"),
              const SizedBox(height: 15),

              ElevatedButton(
                  onPressed: () {
                    questionIndex = 0;
                    questionScreen = true;
                    noOfCorrectAnswers = 0;
                    selectedAnswerIndex = -1;
                    setState(() {});
                  },
                  child: const Text(
                    "Reset",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.normal,
                      color: Colors.orange,
                    ),
                  ))
            ],
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
