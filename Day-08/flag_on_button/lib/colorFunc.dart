import 'package:flutter/material.dart';

int counter = -1;
Widget pole() {
  return Container(height: 500, width: 20, color: Colors.black54);
}

Widget orange() {
  return Container(
    height: 50,
    width: 200,
    color: Colors.orange,
  );
}

Widget white() {
  return Container(
    height: 50,
    width: 200,
    color: Colors.white,
    child: (counter >= 3) ? ashoka() : Container(),
  );
}

Widget ashoka() {
  return Image.network(
      "https://png.pngtree.com/png-vector/20220813/ourlarge/pngtree-ashok-chakra-png-image_6109577.png",
      height: 48,
      width: 48);
}

Widget green() {
  return Container(
    height: 50,
    width: 200,
    color: Colors.green,
  );
}
