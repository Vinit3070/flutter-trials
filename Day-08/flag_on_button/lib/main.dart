import 'package:flutter/material.dart';
import 'package:flag_on_button/colorFunc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Flag(),
    );
  }
}

class Flag extends StatefulWidget {
  const Flag({super.key});

  @override
  State<Flag> createState() => _Flag();
}

class _Flag extends State<Flag> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Indian Flag")),
      body: Container(
        color: Colors.grey,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            (counter >= 0) ? pole() : Container(),
            Column(
              children: [
                (counter >= 1) ? orange() : Container(),
                (counter >= 2) ? white() : Container(),
                (counter >= 4) ? green() : Container(),
                const Spacer(),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        counter++;
                      });
                    },
                    child: const Text("Next")),
                const SizedBox(
                  height: 50,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
