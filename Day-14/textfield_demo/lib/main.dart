// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: textFieldDemo(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class textFieldDemo extends StatefulWidget {
  const textFieldDemo({super.key});

  @override
  State createState() => _textFieldDemoState();
}

class _textFieldDemoState extends State {

  TextEditingController nameC = TextEditingController();
  TextEditingController compC = TextEditingController();
  TextEditingController locC = TextEditingController();

  List cards = [];

  int inc = 0;

  Widget cardsTemp() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: const EdgeInsets.all(5.0),
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            color:const Color.fromARGB(255, 195, 217, 251),
            borderRadius: const BorderRadius.all(Radius.circular(20)),
            boxShadow: const [
              BoxShadow(
                color: Color.fromARGB(255, 195, 250, 181),
                offset: Offset(12, 12),
                blurRadius: 9,
              ),
            ],
            border: Border.all(
              color: Colors.black,
              width: 4,
            )),
        height: 150,
        width: 300,
        child: Column(
          children: [
            const SizedBox(
              height: 15,
            ),
            Text(
              " Name : ${nameC.text}",
              style: const TextStyle(
                fontSize: 15,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              " Company : ${compC.text}",
              style: const TextStyle(
                fontSize: 15,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              " Location : ${locC.text}",
              style: const TextStyle(
                fontSize: 15,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("PortFolio"),
        centerTitle: true,
        backgroundColor: Colors.cyan,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Column(
              children: [
                const SizedBox(
                  height: 40,
                ),
                TextField(
                  autofocus: true,
                  controller: nameC,
                  decoration: const InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    hintText: " Enter Your Name : ",
                    hintStyle: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  keyboardType: TextInputType.multiline,
                ),
                const SizedBox(
                  height: 30,
                ),
                TextField(
                  autofocus: true,
                  controller: compC,
                  decoration: const InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    hintText: " Enter Company Name :  ",
                    hintStyle: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  keyboardType: TextInputType.multiline,
                ),
                const SizedBox(
                  height: 30,
                ),
                TextField(
                  autofocus: true,
                  controller: locC,
                  decoration: const InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    hintText: " Enter Location :  ",
                    hintStyle: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  keyboardType: TextInputType.multiline,
                ),
                const SizedBox(height: 20),
                const Divider(
                  thickness: 8,
                  color: Colors.black
                ),
                const SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      cards.add(inc);
                      inc++;
                    });
                  },
                  child: const Text("SUBMIT"),
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            ),

            const Divider(
              thickness: 8,
              color:Colors.black,
            ),

            // ignore: sized_box_for_whitespace
            SizedBox(
              height: 350,
              child: ListView.builder(
                itemCount: cards.length,
                itemBuilder: ((context, index) {
                  return cardsTemp();
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
