
import 'package:flutter/material.dart';
// import 'package:container_snippets/snipp1.dart';
// import 'package:container_snippets/snipp2.dart';
import 'package:container_snippets/snipp3.dart';
// import 'package:container_snippets/snipp4.dart';
// import 'package:container_snippets/snipp5.dart';
// import 'package:container_snippets/snipp6.dart';
// import 'package:container_snippets/snipp7.dart';
// import 'package:container_snippets/snipp8.dart';
// import 'package:container_snippets/snipp9.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      //home:snipp1(),
      //home:snipp2(),
      home:snipp3(),
      // home:snipp4(),
      // home:snipp5(),
      // home: snipp6(),
      // home: snipp7(),
      // home: snipp8(),
      // home: snipp9(),
      
      debugShowCheckedModeBanner: false,
    );
  }
}
