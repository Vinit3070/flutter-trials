import 'package:flutter/material.dart';

class snipp3 extends StatelessWidget{

  const snipp3({super.key});

  @override
  Widget build (BuildContext context){

    return Scaffold(
      
        appBar: AppBar(
          title: const Text("Container Sizing Parameters -(padding)"),
          backgroundColor: Colors.yellow,
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              bottom: 10,
              top: 10,
            ),
            height: 200,
            width: 200,
            color: Colors.blue,
          ),
        ),
    );
  }
}

