import 'package:flutter/material.dart';

class snipp2 extends StatelessWidget{

  const snipp2({super.key});

  @override
  Widget build (BuildContext context){

    return Scaffold(
      
        appBar: AppBar(
          title: const Text("Container Sizing Parameters -(padding)"),
          backgroundColor: Colors.yellow,
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            height: 200,
            width: 200,
            color: Colors.blue,
          ),
        ),
    );
  }
}

