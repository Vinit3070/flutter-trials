import 'package:flutter/material.dart';

class snipp1 extends StatelessWidget{

  const snipp1({super.key});

  @override
  Widget build (BuildContext context){

    return Scaffold(
      
        appBar: AppBar(
          title: const Text("Container Sizing Parameters -(Height and Width)"),
          backgroundColor: Colors.greenAccent,
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            height: 200,
            width: 200,
            color: Colors.red,
          ),
        ),
    );
  }
}

