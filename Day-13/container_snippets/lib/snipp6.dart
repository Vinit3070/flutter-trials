import 'package:flutter/material.dart';

class snipp6 extends StatelessWidget {
  const snipp6({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Container Sizing Parameters -(Decoration)"),
        backgroundColor: Colors.greenAccent,
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(20),
              ),
              border: Border.all(
                color: Colors.red,
                width: 10,
              )),
        ),
      ),
    );
  }
}
