import 'package:flutter/material.dart';

class snipp4 extends StatelessWidget{

  const snipp4({super.key});

  @override
  Widget build (BuildContext context){

    return Scaffold(
      
        appBar: AppBar(
          title: const Text("Container Sizing Parameters -(margin)"),
          backgroundColor: Colors.greenAccent,
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            margin: const EdgeInsets.only(
              top:10,
              bottom: 10,
              left: 10,
            ),
            height: 200,
            width: 200,
            color: Colors.red,
          ),
        ),
    );
  }
}

