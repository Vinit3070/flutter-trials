import 'package:flutter/material.dart';

class snipp5 extends StatelessWidget {
  const snipp5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Container Sizing Parameters -(decoration)"),
        backgroundColor: Colors.greenAccent,
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
              border: Border.all(
            color: Colors.red,
            width: 5,
          )),
        ),
      ),
    );
  }
}
