import 'package:flutter/material.dart';
//import 'package:listview/listview.dart';
import 'package:listview/assignment1.dart';

void main(){

  runApp(const MyApp());

}

class MyApp extends StatelessWidget{

  const MyApp({super.key});

  @override
  Widget build(BuildContext context){

    return const MaterialApp(

      //home: listview(),
      home: assignment1(),
      
    );
  }
}
