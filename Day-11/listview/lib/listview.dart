
import 'package:flutter/material.dart';

class listview extends StatefulWidget{

  const listview({super.key});

  @override
  State createState() => _MyappState();
}
class _MyappState extends State<listview>{

  List<String> list=[
    "https://bcciplayerimages.s3.ap-south-1.amazonaws.com/ipl/IPLHeadshot2023/6.png",
    "https://bcciplayerimages.s3.ap-south-1.amazonaws.com/ipl/IPLHeadshot2023/2.png",
    "https://www.lucknowsupergiants.in/static-assets/images/players/60122.png?v=5.2",
    "https://images.tv9hindi.com/wp-content/uploads/2022/09/Ravindra-Jadeja-1.jpg",
  ];

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text("IMAGELIST- LISTVIEW- Builder"),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return Container(
            height: 350,
            width:350,
            margin: const EdgeInsets.all(10.0),
            child: Image.network(
              list[index],
            ),
          );
        }

      ),
    );
  }
}