
import 'package:flutter/material.dart';

class assignment1 extends StatefulWidget{

  const assignment1({super.key});

  @override
  State createState() => _MyappState1();
}
class _MyappState1 extends State<assignment1>{

  int counter=0;

  List<String> list=[
   
  ];

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text("Increment Counter- LISTVIEW- Builder"),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {
          return Container(
            width: double.infinity,
            color: Colors.yellow,
            margin: const EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                list[index],
              ),
            ),
          );
        }
      ),
      floatingActionButton: FloatingActionButton(
        onPressed:(){
          setState(() {
            counter++;
            list.add(counter.toString());
          });
        },
        child:const Icon(
          Icons.add,
        ),
      ),
    );
  }
}