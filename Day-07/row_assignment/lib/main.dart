import 'package:flutter/material.dart';

void main(){
  runApp(const MyApp());
}

class MyApp extends StatelessWidget{

  const MyApp({super.key});

  @override
  Widget build (BuildContext context){

    return MaterialApp(
      home:Scaffold(
        appBar: AppBar(
          title: const Text("Row Assignment"),
          backgroundColor: Colors.amber,
        ),
        body: Container(
          height: double.infinity,
          child:Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            
            children: [
              Container(
                height: 100,
                width: 100,
                color: Colors.red,
              ),
              Container(
                height: 100,
                width: 100,
                color: Colors.blue,
              ),
              Container(
                height: 100,
                width: 100,
                color: Colors.green,
              ),
            ],
          ),
        ),
      ),
    );
  }
}