import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Flag Assignment"),
          backgroundColor: Colors.amber,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Row(
            //crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,

            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  
                  children: [
                    Container(
                      height: 500,
                      width: 15,
                      color: Colors.grey,
                    )
                  ],
                ),
              ),
              
              const SizedBox(
                width: 2,
              ),
              Padding(
                  padding: EdgeInsets.only(top: 20,),
                  child: Column(
                    children: [
                      Container(
                        height: 60,
                        width: 300,
                        color: Colors.orangeAccent,
                      ),
                      Container(
                        height: 60,
                        width: 300,
                        color: Colors.white,
                        child:Row(
                          mainAxisAlignment:MainAxisAlignment.center,
                          children: [
                            Image.network("https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Ashoka_Chakra.svg/1024px-Ashoka_Chakra.svg.png",
                            height: 50,
                            width: 50,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 60,
                        width: 300,
                        color: Colors.green,
                      ),
                    ],
                  ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
