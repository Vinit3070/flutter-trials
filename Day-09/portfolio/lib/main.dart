import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Portfolio(),
    );
  }
}

class Portfolio extends StatefulWidget {
  const Portfolio({super.key});

  @override
  State<Portfolio> createState() => _Portfolio();
}

class _Portfolio extends State<Portfolio> {
  int counter = -1;
  @override
  Widget build(BuildContext contex) {
    return Scaffold(
      backgroundColor: Colors.black54,
      appBar: AppBar(
        title: const Text("***** PORTFOLIO *****"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              (counter >= 0)
                  ? const Text("NAME : Vinit Dinesh Nikam")
                  : const Text(""),
              (counter >= 1)
                  ? Container(
                      height: 100,
                      width: 100,
                      child: Image.network(
                          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0sZSUAyIa2MhBrCjAo7jlvthel3Ylr5kyTLLZ32VIxg&s"),
                    )
                  : Container(),
              (counter >= 2)
                  ? const Text(
                      "COLLEGE : Dr.Vithalrao Vikhe Patil COE , Ahmednagar")
                  : const Text(""),
              (counter >= 3)
                  ? Container(
                      height: 100,
                      width: 100,
                      child: Image.network(
                          "https://www.careerindia.com/college-logo/128x128/29/PCMRD_TRANS1_1444633092.jpg"),
                    )
                  : Container(),
              (counter >= 4)
                  ? const Text("DREAM COMPANY : Amazon")
                  : const Text(""),
              (counter >= 5)
                  ? Container(
                      height: 100,
                      width: 100,
                      child: Image.network(
                          "https://tse3.mm.bing.net/th?id=OIP.BEUTMsqITs1iT6S9QzThYAHaHa&pid=Api&P=0&h=180"))
                  : Container(),
              (counter > 5)
                  ? Container()
                  : ElevatedButton(
                      onPressed: () {
                        setState(() {
                          counter++;
                        });
                      },
                      child: const Text("Next")),
              const SizedBox(
                height: 30,
              ),
            ]),
      ),
    );
  }
}
