import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext c) {
    return const MaterialApp(
      home: QuizPage1(),
    );
  }
}

class QuizPage1 extends StatefulWidget {
  const QuizPage1({super.key});

  @override
  State<QuizPage1> createState() => _QuizPage1();
}

class _QuizPage1 extends State<QuizPage1> {
  int page = 1;
  List que = ["Question 1: What is Mixin in dart ?", "question2"];
  @override
  Widget build(BuildContext conntext) {
    return Scaffold(
        backgroundColor: Colors.white38,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.cyan,
          title: const Text("Tech Quiz"),
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text("Question : $page / 10"),
                Text(que[0]),
                SizedBox(
                    height: 30,
                    width: 200,
                    child: ElevatedButton(
                        onPressed: () {}, 
                        child: const Text("Option 1"))),
                SizedBox(
                    height: 30,
                    width: 200,
                    child: ElevatedButton(
                        onPressed: () {},
                        child: const Text("Option 2"))),
                SizedBox(
                    height: 30,
                    width: 200,
                    child: ElevatedButton(
                        onPressed: () {}, 
                        child: const Text("Option 3"))),
                SizedBox(
                    height: 30,
                    width: 200,
                    child: ElevatedButton(
                        onPressed: () {}, 
                        child: const Text("Option 4"))),
                Row(
                  children: [
                    const Spacer(),
                    ElevatedButton.icon(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.navigate_next,
                        color: Colors.black,
                      ),
                      label: const Text(""),
                    ),
                    const SizedBox(
                      width: 40,
                    )
                  ],
                )
              ]),
        ));
  }
}
