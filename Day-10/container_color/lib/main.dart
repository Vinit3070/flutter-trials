import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: colorChange(),
    );
  }
}


class colorChange extends StatefulWidget {
  const colorChange({super.key});

  @override
  State<colorChange> createState() => _colorChange();
}


class _colorChange extends State<colorChange> {
  int counter1 = 0;
  int counter2 = 0;

  Color changeBox1() {
    if (counter1 == 0) {
      return Colors.red;
    } else if (counter1 == 1) {
      return Colors.green;
    } else if (counter1 == 2) {
      return Color.fromARGB(255, 53, 13, 162);
    } else {
      counter1 = 0;
      return const Color.fromARGB(255, 4, 1, 1);
    }
  }

  Color changeBox2() {
    if (counter2 == 0) {
      return Colors.yellow;
    } else if (counter2 == 1) {
      return Colors.purple;
    } else if (counter2 == 2) {
      return Colors.teal;
    } else {
      counter2 = 0;
      return const Color.fromARGB(255, 7, 7, 3);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan,
        appBar: AppBar(
          title: const Text("Change Box Color On Button Click",),centerTitle: true,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: changeBox1(),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        counter1++;
                      });
                    },
    
                    child: const Text("C-1"))
              ],
            ),
            const SizedBox(
              height: 20,
              width: 20,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: changeBox2(),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        counter2++;
                      });
                    },
                    child: const Text("C-2"))
              ],
            )
          ]),
        ));
  }
}
