import 'package:flutter/material.dart';

void main() {
  runApp(const InstaDemo());
}

class InstaDemo extends StatefulWidget {
  const InstaDemo({super.key});

  @override
  State<InstaDemo> createState() => _InstaDemo1State();
}

class _InstaDemo1State extends State<InstaDemo> {

  bool isLike1=true;
  bool isLike2=true;
  bool isLike3=true;
  bool isLike4=true;

  bool isAdded1=true;
  bool isAdded2=true;
  bool isAdded3=true;
  bool isAdded4=true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              title: const Text(
                "Instagram",
                style: TextStyle(
                  fontStyle: FontStyle.normal,
                  color: Colors.black,
                  fontSize: 30,
                ),
              ),
              actions: const [
                // Icon(
                //   Icons.favorite_rounded,
                //   color: Colors.red,
                // ),
              ],
            ),

//LISTVIEW -(It is By default Scrollable..)
          //   body: ListView(

          //     children: [
          //         Column(
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           children: [
          //             Container(
          //               color: Colors.yellow,
          //               child: Image.network(
          //                 "https://tse3.mm.bing.net/th?id=OIP.TnrOwhGnEbJmnZGnccC0ygHaEK&pid=Api&P=0&h=180",
          //                 width: double.infinity,
          //                 height: 200,
          //               ),
          //             ),
          //             Row(
          //               children: [
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.favorite_outline_outlined,
          //                   ),
          //                 ),
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.comment_outlined,
          //                   ),
          //                 ),
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.send,
          //                   ),
          //                 ),
          //               ],
          //             ),
          //           ],
          //         ),
          //         Column(
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           children: [
          //             Container(
          //               color: Colors.green,
          //               child: Image.network(
          //                 "https://tse3.mm.bing.net/th?id=OIP.TnrOwhGnEbJmnZGnccC0ygHaEK&pid=Api&P=0&h=180",
          //                 width: double.infinity,
          //                 height: 200,
          //               ),
          //             ),
          //             Row(
          //               children: [
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.favorite_outline_outlined,
          //                   ),
          //                 ),
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.comment_outlined,
          //                   ),
          //                 ),
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.send,
          //                   ),
          //                 ),
          //               ],
          //             ),
          //           ],
          //         ),
          //         Column(
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           children: [
          //             Container(
          //               color: Colors.cyan,
          //               child: Image.network(
          //                 "https://tse3.mm.bing.net/th?id=OIP.TnrOwhGnEbJmnZGnccC0ygHaEK&pid=Api&P=0&h=180",
          //                 width: double.infinity,
          //                 height: 200,
          //               ),
          //             ),
          //             Row(
          //               children: [
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.favorite_outline_outlined,
          //                   ),
          //                 ),
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.comment_outlined,
          //                   ),
          //                 ),
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.send,
          //                   ),
          //                 ),
          //               ],
          //             ),
          //           ],
          //         ),
          //         Column(
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           crossAxisAlignment: CrossAxisAlignment.start,
          //           children: [
          //             Container(
          //               color: Colors.yellow,
          //               child: Image.network(
          //                 "https://tse3.mm.bing.net/th?id=OIP.TnrOwhGnEbJmnZGnccC0ygHaEK&pid=Api&P=0&h=180",
          //                 width: double.infinity,
          //                 height: 200,
          //               ),
          //             ),
          //             Row(
          //               children: [
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.favorite_outline_outlined,
          //                   ),
          //                 ),
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.comment_outlined,
          //                   ),
          //                 ),
          //                 IconButton(
          //                   onPressed: () {},
          //                   icon: const Icon(
          //                     Icons.send,
          //                   ),
          //                 ),
          //               ],
          //             ),
          //           ],
          //         ),
          //   ],
          // ),

//SingleChildScrollView -

            body: SingleChildScrollView(
              child: Column(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        //color: Colors.yellow,
                        child: Image.network(
                          "https://4.bp.blogspot.com/-7y6JZHHiINc/WlD0Kyz3rjI/AAAAAAAAlrs/Gd0-UDTo8e0Z3kji1fpq4krKT0mumJcNACLcBGAs/s1600/Virat-Kohli-Images-HD-3.jpg",
                          width: double.infinity,
                          height: 200,
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            onPressed: () {

                              setState(() {
                                isLike1=!isLike1;
                              });
                            },
                            icon: isLike1 ?const  Icon(Icons.favorite_border_rounded,): const Icon(Icons.favorite_rounded, color: Colors.red,),

                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.send,
                            ),
                          ),
                          //  const SizedBox(
                          //   width:210,
                          // ),
                          const Spacer(),

                          IconButton(
                            onPressed: () {
                              setState(() {
                                isAdded1=!isAdded1;
                              });
                            },
                             icon: isAdded1 ?const Icon(Icons.bookmark_add_outlined,) : const Icon(Icons.bookmark_added,),

                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        //color: Colors.green,
                        child: Image.network(
                          "https://tse2.mm.bing.net/th?id=OIP.1TEQTmU1ifCcnUWIeL9CzAHaEo&pid=Api&P=0&h=180",
                          width: double.infinity,
                          height: 200,
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            onPressed: () {

                              setState(() {
                                isLike2=!isLike2;
                              });
                            },
                            icon: isLike2 ?const  Icon(Icons.favorite_border_rounded,): const Icon(Icons.favorite_rounded, color: Colors.red,),

                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.send,
                            ),
                          ),
                          //  const SizedBox(
                          //   width:210,
                          // ),
                          const Spacer(),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                isAdded2=!isAdded2;
                              });
                            },
                            icon: isAdded2 ?const Icon(Icons.bookmark_add_outlined,): const Icon(Icons.bookmark_added,),

                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                       // color: Colors.cyan,
                        child: Image.network(
                          "https://www.thefamouspeople.com/profiles/images/rohit-sharma-6.jpg",
                          width: double.infinity,
                          height: 200,
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            onPressed: () {

                              setState(() {
                                isLike3=!isLike3;
                              });
                            },
                            icon: isLike3 ?const  Icon(Icons.favorite_border_rounded,): const Icon(Icons.favorite_rounded, color: Colors.red,),

                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.send,
                            ),
                          ),
                          //  const SizedBox(
                          //   width:210,
                          // ),

                          const Spacer(),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                isAdded3=!isAdded3;
                              });
                            },
                             icon: isAdded3 ?const Icon(Icons.bookmark_add_outlined,): const Icon(Icons.bookmark_added),

                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        //color: Colors.yellow,
                        child: Image.network(
                          "https://wallpaperaccess.com/full/4579575.jpg",
                          width: double.infinity,
                          height: 200,
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            onPressed: () {

                              setState(() {
                                isLike4=!isLike4;
                              });
                            },
                            icon: isLike4 ?const  Icon(Icons.favorite_border_rounded,): const Icon(Icons.favorite_rounded, color: Colors.red,),

                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.comment_outlined,
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.send,
                            ),
                          ),
                          //  const SizedBox(
                          //   width:210,
                          // ),
                          
                          const Spacer(),

                         IconButton(
                            onPressed: () {
                              setState(() {
                                isAdded4=!isAdded4;
                              });
                            },
                            icon: isAdded4 ?const Icon(Icons.bookmark_add_outlined,): const Icon(Icons.bookmark_added,),

                          ),
                        ],
                      ),
                    ],
                  ),
              ],
           ),
        )

      )
    );
  }
}
