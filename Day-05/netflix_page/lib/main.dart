import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.red,
            title: const Text("Netflix"),
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [

                const Text("Movies"),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Image.network(
                        "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",
                        height: 400,
                        width: 300,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Image.network(
                        "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png",
                        height: 400,
                        width: 300,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Image.network(
                        "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",
                        height: 400,
                        width: 300,
                      ),
                    ],
                  ),
                ),

                const Text("Web Series"),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Image.network(
                        "https://assetscdn1.paytm.com/images/catalog/product/H/HO/HOMSHERLOCK-HOLHK-P63024784A1CC1B/1563111214645_0..jpg",
                        height: 400,
                        width: 300,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Image.network(
                        "https://www.tallengestore.com/cdn/shop/products/PeakyBlinders-NetflixTVShow-ArtPoster_125897c4-6348-41e8-b195-d203700ebcca.jpg?v=1619864555",
                        height: 400,
                        width: 300,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Image.network(
                        "https://dnm.nflximg.net/api/v6/2DuQlx0fM4wd1nzqm5BFBi6ILa8/AAAAQeIeKt7LlqIJPKrT4aQijclj7K43xRSU3dQXNESNdNbnnJbT6LLWVRT9srUUbHbOo-iOH-8v3o16pUDMQ6tCgNGlkvfwvDOprROIZpQ2rgHtop9rHvbYlvzavMmUSGBCXjynJ80dn4nqZzZmzIUJMQpS.jpg?r=943",
                        height: 400,
                        width: 300,
                      ),
                    ],
                  ),
                ),

                const Text("Popular Movies",textAlign: TextAlign.start,),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Image.network(
                        "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png",
                        height: 400,
                        width: 300,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Image.network(
                        "https://dnm.nflximg.net/api/v6/2DuQlx0fM4wd1nzqm5BFBi6ILa8/AAAAQeIeKt7LlqIJPKrT4aQijclj7K43xRSU3dQXNESNdNbnnJbT6LLWVRT9srUUbHbOo-iOH-8v3o16pUDMQ6tCgNGlkvfwvDOprROIZpQ2rgHtop9rHvbYlvzavMmUSGBCXjynJ80dn4nqZzZmzIUJMQpS.jpg?r=943",
                        height: 400,
                        width: 300,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Image.network(
                        "https://dbdzm869oupei.cloudfront.net/img/posters/preview/91008.png",
                        height: 400,
                        width: 300,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ),
    );
  }
}
