import 'package:flutter/material.dart';


class Assignment3 extends StatefulWidget{

  const Assignment3({super.key});

  @override
  State<Assignment3> createState()=>_Assignment3State();
}
class _Assignment3State extends State<Assignment3>{

  int? selectedIndex=0;

  final List<String> imageList=[
      "https://i2.wp.com/techbeasts.com/wp-content/uploads/2016/01/green_mountain_nature_wallpaper_hd.jpg",
      "https://tse2.mm.bing.net/th?id=OIF.pxOyPCQs1dWUmJdr1Fhp2Q&pid=Api&P=0&h=180",
      "https://tse3.mm.bing.net/th?id=OIF.jmV2hO2r9NpDageVu%2b45Rg&pid=Api&P=0&h=180"
  ];

  void showNextImage(){
    setState(() {
      selectedIndex=(selectedIndex == imageList.length-1)
      ? selectedIndex =0 : selectedIndex! +1;
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.grey.shade400,
      appBar: AppBar(
        title: const Text("**** Display Images ****"),
      ),

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
              imageList[selectedIndex!],
              width: 300,
              height:300,
            ),
            const SizedBox(
              height:20,
            ),
            ElevatedButton(
              onPressed: showNextImage,
              child:const Text("Next"),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: (){
                setState(() {
                  selectedIndex=0;
                });
              },
             child: const Text("Reset"),
            )  
          ],
        ),
      ),
      
    );
  }
  
}