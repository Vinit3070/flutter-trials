import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget{

    const Assignment1({super.key});
     @override
    State<Assignment1> createState()=>_Assignment1State();

}

class _Assignment1State extends State<Assignment1>{

  final List<int> palindromeNum=[121,32323,100,99,2030302];
  final List<int> armstrongNum=[153,1634,10,370,60302];
  final List<int> strongNum=[1,145,234,751];

  int? pCnt=0;
  int? aCnt=0;
  int? sCnt=0;

  void palindromeNumber(){
    setState(() {

        if(pCnt! > 0){
          pCnt=0;
        }

      
      for(int n in palindromeNum){

        int rev=0;
        int temp=n;

        while(n!=0){

          int rem=n%10;
          rev=rev*10+rem;
          n=n~/10;

        }

        if(rev==temp){
          pCnt = pCnt! +1;
        }
      }
    });
  }

  void armstrongNumber(){
    setState(() {
      
        
          aCnt=0;
        

        for(int n in armstrongNum){
          int sum=0;
          int dCnt=0;

          for(int i=n;i!=0;i=i~/10){
            dCnt++;
          }

          for(int i=n; i!=0; i=i~/10){

            int prod=1;
            for(int j=1;j<=dCnt;j++){
              prod=prod*(i%10);
            }
            sum=sum+prod;
          }
        if(sum==n){
          aCnt=aCnt!+1;
        }
      }
    });
  }

  void strongNumber(){
    setState(() {
      
        if(sCnt! > 0){
          sCnt=0;
        }

        for(int n in strongNum){
          int sum=0;
          for(int i=n;i!=0;i=i~/10){
            int fact=1;
            for(int j=2;j<=i%10;j++){
              fact=fact*j;

            }
            sum=sum+fact;
          }

          if(sum==n){
            sCnt=sCnt!+1;
          }
        }
    });
  }

    @override
    Widget build(BuildContext context){
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Number Count"
          ),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [

            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "List of Palindrome Number:: $palindromeNum"
                  ),
                  const SizedBox(
                    height:20,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: palindromeNumber,
                    child: const Text(
                      "Check Palindrome"
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    "$pCnt",
                  ),
                ],
              ),
              const SizedBox(
              height: 50,
              ),

            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "List of Armstrong Numbers:: $armstrongNum"
                  ),
                  const SizedBox(
                    height:20,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: armstrongNumber,
                    child: const Text(
                      "Check Armstrong Number"
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    "$aCnt",
                  ),
                ],
              ),
              const SizedBox(
              height: 50,
              ),
            
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "List of Strong Number:: $strongNum"
                  ),
                  const SizedBox(
                    height:20,
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: strongNumber,
                    child: const Text(
                      "Check Strong Number:"
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    "$sCnt",
                  ),
                ],
              ),
              const SizedBox(
              height: 50,
              ),
            ],
          ),
        
          
        ),
      ); 
    }
}
