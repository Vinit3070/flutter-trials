import 'package:flutter/material.dart';

class assignment7 extends StatelessWidget {
  const assignment7({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
      
      child: Center(
        
        child: Column(
        children: [
          const SizedBox(
            width: 20,
          ),
          Image.network(
            "https://tse2.mm.bing.net/th?id=OIP.deFNQkxNcq8Oxjl82KMDvAHaE8&pid=Api&P=0&h=180",
            height: 400,
            width: 400,
          ),
          const SizedBox(
            width: 20,
          ),
          Image.network(
            "https://tse4.mm.bing.net/th?id=OIP.defmtwl1SFwOTbtE3mV8cAHaEc&pid=Api&P=0&h=180",
            height: 400,
            width: 400,
          ),
          const SizedBox(
            width: 20,
          ),
          Image.network(
            "https://tse2.mm.bing.net/th?id=OIP.O_E20ZomX30r1IhSBvh11AHaFj&pid=Api&P=0&h=180",
            height: 400,
            width: 400,
          ),
          const SizedBox(
            width: 20,
          ),
        ],
      ),
    )));
  }
}
