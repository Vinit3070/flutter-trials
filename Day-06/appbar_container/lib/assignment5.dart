import 'package:flutter/material.dart';

class assignment5 extends StatelessWidget {
  const assignment5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Center(
          child: Row(
            children: [
              const SizedBox(
                width: 20,
              ),
              Image.asset(
                "assets/hyabusa.jpeg",
                height: 500,
                width: 500,
              ),
              const SizedBox(
                width: 20,
              ),
              Image.asset(
                "assets/bmw310rr.jpeg",
                height: 500,
                width: 500,
              ),
              const SizedBox(
                width: 20,
              ),
              Image.asset(
                "assets/ZX10R.jpeg",
                height: 500,
                width: 500,
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}
