// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'assignment1.dart';
import 'assignment2.dart';
import 'assignment3.dart';
import 'assignment4.dart';
import 'assignment5.dart';
import 'assignment6.dart';
import 'assignment7.dart';
import 'padding_margin.dart';
import 'Cassignment1.dart';


void main(){

  runApp(const MyApp());
}
class MyApp extends StatelessWidget{

  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      // home: assignment1(),
      // home: assignment2(),
      // home: assignment3(),
      // home: assignment4(),
      // home: assignment5(),
      // home: assignment6(),
      // home: assignment7(),
      // home: PaddingAssignment(),
      home: Cassignment1(),
      // home: Row(),
      // //home: Column(),
      debugShowCheckedModeBanner: false,

    );
  }
}