import 'package:flutter/material.dart';

class assignment1 extends StatelessWidget{

  const assignment1({super.key});

  @override
  Widget build (BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Assignment1",
          style: TextStyle(
            fontStyle: FontStyle.normal,
            fontSize: 20,
          ),
        ),
        backgroundColor: Colors.cyan,
        actions: const [
          Icon(
            Icons.favorite,
          ),
           SizedBox(
            width: 20,
          ),
          Icon(
            Icons.face,
          ),
           SizedBox(
            width: 20,
          ),
        ],
      ),
      
    );
  }
}

