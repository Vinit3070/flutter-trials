import 'package:flutter/material.dart';

class assignment4 extends StatelessWidget{

  const assignment4({super.key});

  @override
  Widget build (BuildContext context){

    return Scaffold(
      body: Center(
        child:
        Column(
        children:[
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 300,
            width: 360,
            color:Colors.blue,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
          height: 200,
          width: 360,
          color:Colors.red,
        ),
        ],
        ),
      ),
    
    );
  }
}

