import 'package:flutter/material.dart';

class assignment2 extends StatelessWidget{

  const assignment2({super.key});

  @override
  Widget build (BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Assignment2",
          style: TextStyle(
            fontStyle: FontStyle.normal,
            fontSize: 20,
          ),
        ),
        backgroundColor: Colors.lightGreen,
        centerTitle: true,
        actions: const [
          Icon(
            Icons.favorite,
          ),
           SizedBox(
            width: 20,
          ),
          Icon(
            Icons.face,
          ),
           SizedBox(
            width: 20,
          ),
        ],
      ),
      
      
    );
  }
}

