import 'package:flutter/material.dart';

class assignment6 extends StatelessWidget {
  const assignment6({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
        child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 300,
              width: 360,
              color: Colors.blue,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 200,
              width: 360,
              color: Colors.red,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 300,
              width: 360,
              color: Colors.blue,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 200,
              width: 360,
              color: Colors.red,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 300,
              width: 360,
              color: Colors.blue,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 200,
              width: 360,
              color: Colors.red,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 300,
              width: 360,
              color: Colors.blue,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 200,
              width: 360,
              color: Colors.red,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 300,
              width: 360,
              color: Colors.blue,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 200,
              width: 360,
              color: Colors.red,
            ),
          ],
        ),
      ),
    ));
  }
}