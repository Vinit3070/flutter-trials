// ignore_for_file: camel_case_types, sized_box_for_whitespace

import 'package:flutter/material.dart';

class task3 extends StatefulWidget {
  const task3({super.key});

  @override
  State createState() => _task3State();
}

class _task3State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Container(
            alignment: Alignment.center,
            child: const Text(
              "Flutter",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          backgroundColor: const Color.fromARGB(255, 210, 210, 88)),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: 100,
          width: 100,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: const Color.fromARGB(255, 90, 147, 239),
              border:
                  Border.all(color: const Color.fromARGB(255, 226, 90, 239),width: 3),
              borderRadius:
                  const BorderRadius.only(topRight: Radius.circular(30))),
        ),
      ),
    );
  }
}
