// ignore_for_file: camel_case_types, sized_box_for_whitespace

import 'package:flutter/material.dart';

class task1 extends StatefulWidget {
  const task1({super.key});

  @override
  State createState() => _task1State();
}

class _task1State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Container(
            alignment: Alignment.center,
            child: const Text(
              "Flutter",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          backgroundColor: const Color.fromARGB(255, 210, 210, 88)),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: const BorderRadius.all(Radius.circular(30)),
            border:Border.all(
              color: Colors.red,
              width: 5,
            )
          ),
          child: const Text("Hello",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w700)),
        ),
      ),
    );
  }
}
