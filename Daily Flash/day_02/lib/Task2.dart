// ignore_for_file: camel_case_types, sized_box_for_whitespace

import 'package:flutter/material.dart';

class task2 extends StatefulWidget {
  const task2({super.key});

  @override
  State createState() => _task2State();
}

class _task2State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Container(
            alignment: Alignment.center,
            child: const Text(
              "Flutter",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          backgroundColor: const Color.fromARGB(255, 210, 210, 88)),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: 100,
          width: 100,
          alignment: Alignment.center,
          decoration:const  BoxDecoration(
            color: Colors.blue,
            border: Border(
              left: BorderSide(
                color: Colors.red,
                width: 5,
              ),
            ),
            borderRadius: BorderRadius.all(Radius.circular(30)),
          ),
          child: const Text("Hello",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
        ),
      ),
    );
  }
}
