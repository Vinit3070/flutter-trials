// ignore_for_file: camel_case_types, sized_box_for_whitespace

import 'package:flutter/material.dart';

class task4 extends StatefulWidget {
  const task4({super.key});

  @override
  State createState() => _task4State();
}

class _task4State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Container(
            alignment: Alignment.center,
            child: const Text(
              "Flutter",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          backgroundColor: const Color.fromARGB(255, 210, 210, 88)),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(10),
          height: 100,
          width: 200,
          // alignment: Alignment.center,
          decoration: BoxDecoration(
              color: const Color.fromARGB(255, 238, 99, 219),
              border:
                  Border.all(color: const Color.fromARGB(255, 143, 38, 222),width: 3),
              borderRadius:
                  const BorderRadius.only(topLeft: Radius.circular(30),bottomRight: Radius.circular(30))
          
          ),

          child: const Text("Vinit Nikam"),
            
        ),
      ),
    );
  }
}
