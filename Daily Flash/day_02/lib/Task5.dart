// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

class task5 extends StatefulWidget {
  const task5({super.key});

  @override
  State createState() => _task5State();
}

class _task5State extends State<task5> {
  String _displayText = "Click me!";
  Color _containerColor = Colors.red;

  void _handleTap() {
    setState(() {
      if (_displayText == "Click me!") {
        _displayText = "Container Tapped";
        _containerColor = Colors.blue;
      } else {
        _displayText = "Click me!";
        _containerColor = Colors.red;
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter Demo"),
        centerTitle: true,
        backgroundColor: Colors.cyan,
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          color: _containerColor,
          child: GestureDetector(
            onTap: _handleTap,
            child: Center(
              child: Text(
                _displayText,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
