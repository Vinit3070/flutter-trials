// ignore_for_file: unused_import

// import 'package:day_02/Task2.dart';
// import 'package:day_02/Task4.dart';
import 'package:day_02/Task5.dart';
import 'package:flutter/material.dart';
// import 'package:day_02/Task1.dart';
// import 'package:day_02/Task3.dart';

void main(){
  runApp(const MyApp());
}

class MyApp extends StatelessWidget{

  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
    
    return const MaterialApp(
      // home:task1(),
      // home:task2(),
      // home:task3(),
      // home:task4(),
      home:task5(),
      debugShowCheckedModeBanner: false,
    );
  }
}