// ignore_for_file: camel_case_types, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class task4 extends StatefulWidget {
  const task4({super.key});

  @override
  State createState() => _task4State();
}

class _task4State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Container(
            alignment: Alignment.center,
            child: const Text(
              "Flutter",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          backgroundColor: Colors.green),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration:BoxDecoration(
            color: Colors.blue,
            border: Border.all(
              color: Colors.red,
              width: 8,
            ),
          ),
        ),
        
      ),
    );
  }
}
