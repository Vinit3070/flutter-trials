// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

class task2 extends StatefulWidget{

  const task2({super.key});

  @override
  State createState() => _task2State();
}

class _task2State extends State{

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon:const Icon(Icons.menu),
          tooltip: 'Menu Icon',
          onPressed:() {},     
        ),

        title: Container(
          alignment: Alignment.center,
          child: const Text(
            "Flutter",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        
        actions: [
          IconButton(
            icon: const Icon(Icons.add_box),
            tooltip: 'Add Icon',
            onPressed: () {},   
          ), 
          IconButton(
            icon: const Icon(Icons.settings),
            tooltip: 'Setting Icon',
            onPressed: () {},
          ), 
          IconButton(
            icon: const Icon(Icons.chat),
            tooltip: 'Chat Icon',
            onPressed: () {},
          ), 
        ],
        backgroundColor:const Color.fromARGB(255, 129, 217, 79),
      ),
    );
  }
}