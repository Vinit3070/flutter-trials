// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

class task1 extends StatefulWidget{

  const task1({super.key});

  @override
  State createState() => _task1State();
}

class _task1State extends State{

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(

        leading: IconButton(
          icon:const Icon(Icons.menu),
          tooltip: 'Menu Icon',
          onPressed:() {},     
        ),

        title: Container(
          alignment: Alignment.center,
          child: const Text(
            "Hello",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        
        actions: [

          IconButton(
            icon: const Icon(Icons.comment),
            tooltip: 'Comment Icon',
            onPressed: () {},   
          ), 
          IconButton(
            icon: const Icon(Icons.settings),
            tooltip: 'Setting Icon',
            onPressed: () {},
          ), 
        ],
        backgroundColor:const Color.fromARGB(255, 34, 150, 148),
      ),
    );
  }
}