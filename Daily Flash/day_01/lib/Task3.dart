// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

class task3 extends StatefulWidget {
  const task3({super.key});

  @override
  State createState() => _task3State();
}

class _task3State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          alignment: Alignment.center,
          child: const Text(
            "Flutter",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        shape: const RoundedRectangleBorder(
          // side: BorderSide(
          //   color: Colors.black,
          //   width: 5,
          // ),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
        backgroundColor: const Color.fromARGB(255, 79, 203, 223),
      ),
    );
  }
}
