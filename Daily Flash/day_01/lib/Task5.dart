// ignore_for_file: camel_case_types, sized_box_for_whitespace

import 'package:flutter/material.dart';

class task5 extends StatefulWidget {
  const task5({super.key});

  @override
  State createState() => _task5State();
}

class _task5State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Container(
            alignment: Alignment.center,
            child: const Text(
              "Flutter",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          backgroundColor: Colors.green),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.all(Radius.circular(30)),
            boxShadow:  [
              BoxShadow(
                color: Colors.red,
                blurRadius: 15,
                spreadRadius: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
